# This script takes in images from a folder and make a crossfade video from the images using ffmpeg.
# Make sure you have ffmpeg installed before running.

# The output command looks something like the below, but for as many images as you have in the folder.
# See the answer by LordNeckbeard at:
# http://superuser.com/questions/833232/create-video-with-5-images-with-fadein-out-effect-in-ffmpeg/1071748#1071748
#
#
# ffmpeg \
# -loop 1 -t 1 -i 001.png \
# -loop 1 -t 1 -i 002.png \
# -loop 1 -t 1 -i 003.png \
# -loop 1 -t 1 -i 004.png \
# -loop 1 -t 1 -i 005.png \
# -filter_complex \
# "[1:v][0:v]blend=all_expr='A*(if(gte(T,0.5),1,T/0.5))+B*(1-(if(gte(T,0.5),1,T/0.5)))'[b1v]; \
# [2:v][1:v]blend=all_expr='A*(if(gte(T,0.5),1,T/0.5))+B*(1-(if(gte(T,0.5),1,T/0.5)))'[b2v]; \
# [3:v][2:v]blend=all_expr='A*(if(gte(T,0.5),1,T/0.5))+B*(1-(if(gte(T,0.5),1,T/0.5)))'[b3v]; \
# [4:v][3:v]blend=all_expr='A*(if(gte(T,0.5),1,T/0.5))+B*(1-(if(gte(T,0.5),1,T/0.5)))'[b4v]; \
# [0:v][b1v][1:v][b2v][2:v][b3v][3:v][b4v][4:v]concat=n=9:v=1:a=0,format=yuv420p[v]" -map "[v]" out.mp4


import os
import subprocess
# SETTINGS
input_dir = "D:\\vritt" 
output_file = "video_1.mp4"  # Name of output video
crossfade = 0.9  # Crossfade duration between two images
delay = 1  # Delay per image


def write_script(files):
        inputs = ""
        filters = ""
        outputs = "[0:v]"
        for i, f in enumerate(files):
                next_f = i + 1
                inputs += ' -loop 1 -t {0} -i {1}'.format(delay, f)
                if not i == len(files) - 1:
                        filters += " [{0}:v][{1}:v]blend=all_expr='A*(if(gte(T,{2}),1,T/{2}))+B*(1-(if(gte(T,{2}),1,T/{2})))'[b{0}v];".format(next_f, i, crossfade)
                if i > 0:
                        outputs += '[b{0}v][{0}:v]'.format(i)

        outputs += 'concat=n={0}:v=1:a=0,format=yuv420p[v]\" -map \"[v]\"  -c:v libx264 -r 15 -crf 23 {1}'.format(str(len(files) * 2 - 1), output_file)

        script = 'ffmpeg {0} -filter_complex \"{1} {2}"'.format(inputs, filters, outputs)
        #print(script)
        subprocess.call(script, shell = True)

#def resizing_images(images):
 #       command = ""
#        for image in images:
#                command += ""
def open_folder(input_dir):
        files = []
        #need to add os.chdir
        for f in os.listdir(input_dir):
                if f.endswith(".jpg"):
                        files.append(f)
        write_script(files)


open_folder(input_dir)
'''
if __name__ == "__main__":
        open_folder(input_dir)'''
