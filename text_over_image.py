#this script helps write text overlays over a video
#the function for text overlay is as follows:
# ffmpeg -i INPUT IMAGE -vf "drawtext=fontfile=PATH_TO_FONT_FILE(C:\Windows\Fonts):textfile=TEXTFILE_TO_BE_OVERLAYED:fontcolor=COLOR(default=black)@OPACITY:fontsize=SIZE(default=16)x:LOCATION_X:y=LOCATION_Y" OUTPUT_VIDEO 

import argparse
import os
import subprocess

def moving_text_overlay(in_i,text_f,out_v,font_file,speed):
	command = "ffmpeg -i {0} -filter_complex \"drawtext=fontfile={1}:fontcolor=black@1:x=w/{4}*mod(t\,{4}):y=(H-th)/2:textfile={2}:reload=1\" {3}".format(in_i,font_file,text_f,out_v,speed)
	subprocess.call(command,shell=True)

def static_text_overlay(in_i,text_f,out_v,font_file):
	command = "ffmpeg -i {0} -filter_complex \"drawtext=fontfile={1}:fontcolor=black@1:x=(w-tw)/2:y=(H-th)/2:textfile={2}:reload=1\" {3}".format(in_i,font_file,text_f,out_v)
	subprocess.call(command,shell=True)
	
parser = argparse.ArgumentParser(description='Text overlay script')
parser.add_argument("mode", action="store", help="Mode of textoverlay i.e. s/S for static and m/M for moving")
parser.add_argument("i", action="store", help="Input image file")
parser.add_argument("t", action="store", help="Text overlay file")
parser.add_argument("o", action="store", help="Output video file")
parser.add_argument("-s", action="store", help="speed of moving = total time taken by one line of text to move from one end to another")

args = parser.parse_args()

font_file = "arial.ttf"
if args.mode=='s' or args.mode=="S":
	static_text_overlay(args.i,args.t,args.o,font_file)
elif args.mode=='m' or args.mode=="M":
	moving_text_overlay(args.i,args.t,args.o,font_file,args.s)