# This script takes an image from a folder and embeds a video on that image. The output video lasts as long as the embedded video lasts.
# Make sure you have ffmpeg installed before running.

#video on image can be done using an overlay filter
#command to generate a video on image is given at http://superuser.com/questions/1068447/ffmpeg-overlaying-scaled-video-over-image/1068531
#it is:
#ffmpeg -i INPUT_IMAGE -vf "movie=INEER_SMALL_VIDEO[inner]; [in][inner] overlay=X:Y [out]" OUTPUT_VIDEO
#INPUT_IMAGE being the image over which a smaller video would be embedded
#INNER_SMALL_VIDEO being the video to be embedded
#X:Y padding across the smaller video

#for adjusting two video on a single image the following command is used.
#ffmpeg -i INPUT_IMAGE -vf "movie=INNER_SMALL_VIDEO_1[inner1];movie=INNER_SMALL_VIDEO_2[inner2]; [in][inner1] overlay=X1:Y1 [out1]; [out1][inner2] overlay=X2:Y2 [out]" OUTPUT_VIDEO
#INPUT_IMAGE being the image over which a smaller video would be embedded
#INNER_SMALL_VIDEO_1/INNER_SMALL_VIDEO_2 being the videos to be embedded
#X1:Y1/X2:Y2 paddings across the smaller videos


import argparse
import os
import subprocess

def single_video_on_image(in_i,embedded,out_v,x,y):
        command = "ffmpeg -i {0} -vf \"movie={1}[inner];[in][inner]overlay={3}:{4}[out]\" {2}".format(in_i,embedded,out_v,x,y)
        subprocess.call(command,shell=True)

def two_videos_on_image(in_i,embedded_1,embedded_2,out_v,x1,y1,x2,y2):
		command = "ffmpeg -i {0} -vf \"movie={1}[inner1]; movie={2}[inner2]; [in][inner1]overlay={4}:{5}[out1]; [out1][inner2]overlay={6}:{7}[out]\" {3}".format(in_i,embedded_1,embedded_2,out_v,x1,y1,x2,y2)
		subprocess.call(command,shell=True)


parser = argparse.ArgumentParser(description='Video resolution converter script')
parser.add_argument("n", action="store", help="number of videos to embed",type=int)
parser.add_argument("i", action="store", help="path to input image")
parser.add_argument("-e1", action="store", help="path to the being 1st embedded video")
parser.add_argument("-e2", action="store", help="path to the being 2nd embedded video")
parser.add_argument("o", action="store", help="path to output video")
parser.add_argument("-x1", action="store", help="X offset for the 1st embedded video")
parser.add_argument("-y1", action="store", help="y offset for the 1st embedded video")
parser.add_argument("-x2", action="store", help="X offset for the 2nd embedded video")
parser.add_argument("-y2", action="store", help="y offset for the 2nd embedded video")

args = parser.parse_args()
if args.n ==1:
	single_video_on_image(args.i,args.e1,args.o,args.x1,args.y1)
elif args.n==2:
	two_videos_on_image(args.i,args.e1,args.e2,args.o,args.x1,args.y1,args.x2,args.y2)

'''
if __name__ == "__main__":
        open_folder(input_dir)'''
