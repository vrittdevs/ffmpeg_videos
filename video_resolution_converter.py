#script to convert resolution of a video
#the command to convert a video to a resolution X*Y is:
#ffmpeg -i INPUT_VIDEO -vf scale=X:Y OUTPUT_VIDEO
#INPUT_VIDEO being the video to be converted
#OUTPUT_VIDEO being the converted video
#X*Y being the resolution to be converted to 

import argparse
import os
import subprocess

def converter(input,output, res_x, res_y):
	command = "ffmpeg -i {0} -vf scale={2}:{3} {1}".format(input,output, res_x, res_y)
	subprocess.call(command,shell=True)

parser = argparse.ArgumentParser(description='Video resolution converter script')

parser.add_argument("i", action="store", help="path to input video")
parser.add_argument("o", action="store", help="path to output video")
parser.add_argument("x", action="store",default=320, type=int, help="resolution X")
parser.add_argument("y", action="store",default=320, type=int,help="resolution Y")

args = parser.parse_args()
converter(args.i,args.o,args.x,args.y)
